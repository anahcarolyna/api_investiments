package br.com.investimento.models.dtos;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class SimulacaoRespostaCTO {
    private double rendimentoPorMes;
    private double montante;

    public SimulacaoRespostaCTO(int rendimentoPorMes, double montante) {
        this.rendimentoPorMes = rendimentoPorMes;

        this.montante = montante;
    }

    public SimulacaoRespostaCTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
        return montante;
    }

    public void setMontante(double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }
}
