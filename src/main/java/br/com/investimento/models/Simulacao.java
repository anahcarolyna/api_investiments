package br.com.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_simulacao;

    @NotNull(message = "É obrigatório o preenchimento do nome")
    private String nomeInteressado;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "É obrigatório o preenchimento do email")
    private String email;

    @NotNull(message = "É obrigatório o preenchimento do valor")
    @Digits(integer = 10, fraction = 2 ,message = "Valor está no formato incorreto")
    private double valorAplicado;

    @NotNull(message = "É obrigatório o preenchimento do número de meses para simulação")
    @Digits(integer = 4, fraction = 0, message = "Valor inválido")
    private int quantidadeMeses;

    @ManyToOne(cascade = CascadeType.ALL)
    private TipoInvestimento TipoInvestimento;

    public Simulacao() {
    }

    public Simulacao(int id_simulacao, @NotNull(message = "É obrigatório o preenchimento do nome") String nomeInteressado, @Email(message = "O formato do email é inválido") @NotNull(message = "É obrigatório o preenchimento do email") String email, @NotNull(message = "É obrigatório o preenchimento do valor") @Digits(integer = 10, fraction = 2) double valorAplicado, @NotNull(message = "É obrigatório o preenchimento do número de meses para simulação") int quantidadeMeses, br.com.investimento.models.TipoInvestimento tipoInvestimento) {
        this.id_simulacao = id_simulacao;
        this.nomeInteressado = nomeInteressado;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
        TipoInvestimento = tipoInvestimento;
    }

    public int getId_simulacao() {
        return id_simulacao;
    }

    public void setId_simulacao(int id_simulacao) {
        this.id_simulacao = id_simulacao;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado)
    {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public double getValorAplicado() {
        BigDecimal bd = new BigDecimal(valorAplicado).setScale(2, RoundingMode.FLOOR);
        this.valorAplicado = bd.doubleValue();
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        BigDecimal bd = new BigDecimal(valorAplicado).setScale(2, RoundingMode.FLOOR);
        this.valorAplicado = bd.doubleValue();
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {

        this.quantidadeMeses = quantidadeMeses;
    }

    public TipoInvestimento getTipoInvestimento() {

        return TipoInvestimento;
    }

    public void setTipoInvestimento(TipoInvestimento tipoInvestimento)
    {
        TipoInvestimento = tipoInvestimento;
    }
}
