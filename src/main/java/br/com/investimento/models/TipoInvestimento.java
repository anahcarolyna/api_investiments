package br.com.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

@Entity
public class TipoInvestimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    @NotNull(message = "O campo nome é obrigatório")
    private String nome;

    @DecimalMin( value = "0.001", message = "O valor está abaixo do valor mínimo de preenchimento")
    @DecimalMax(value = "10.0",  message = "O valor está acima do valor máximo de preenchimento")
    @Digits(integer = 10, fraction = 5 ,message = "Valor está no formato incorreto")
    private double rendimentoAoMes;

    public TipoInvestimento() {
    }

    public TipoInvestimento(String nome, double rendimentoAoMes) {
        this.nome = nome;
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public TipoInvestimento(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
