package br.com.investimento.exceptions.errors;

import java.util.HashMap;

public class MensagemDeErro {
    private String erro;
    private String mensagemDeErro;
    private HashMap<String, ObjetoDeErro> camposdeErro;

    public MensagemDeErro() {
    }

    public MensagemDeErro(String erro, String mensagemDeErro, HashMap<String, ObjetoDeErro> camposdeErro) {
        this.erro = erro;
        this.mensagemDeErro = mensagemDeErro;
        this.camposdeErro = camposdeErro;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemDeErro() {
        return mensagemDeErro;
    }

    public void setMensagemDeErro(String mensagemDeErro) {
        this.mensagemDeErro = mensagemDeErro;
    }

    public HashMap<String, ObjetoDeErro> getCamposdeErro() {
        return camposdeErro;
    }

    public void setCamposdeErro(HashMap<String, ObjetoDeErro> camposdeErro) {
        this.camposdeErro = camposdeErro;
    }
}
