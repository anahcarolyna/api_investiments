package br.com.investimento.controllers;

import br.com.investimento.models.Simulacao;
import br.com.investimento.models.TipoInvestimento;
import br.com.investimento.models.dtos.SimulacaoRespostaCTO;
import br.com.investimento.service.SimulacaoService;
import br.com.investimento.service.TipoInvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class TipoInvestimentoController {

    @Autowired
    private TipoInvestimentoService tipoInvestimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TipoInvestimento salvarTipoInvestimento(@RequestBody @Valid TipoInvestimento tipoInvestimento){
        return tipoInvestimentoService.salvarInvestimento(tipoInvestimento);
    }

    @GetMapping
    public Iterable<TipoInvestimento> consultarTodos(){
        Iterable<TipoInvestimento> tipoInvestimentos = tipoInvestimentoService.consultarTodosInvestimentos();

        return tipoInvestimentos;
    }

    @PostMapping("/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.OK)
    public SimulacaoRespostaCTO simularInvestimento(@PathVariable(name = "idInvestimento") int id, @RequestBody @Valid Simulacao simulacao){

        TipoInvestimento tipoInvestimento = tipoInvestimentoService.buscarPorId(id);

        simulacao.setTipoInvestimento(tipoInvestimento);
        SimulacaoRespostaCTO simulacaoRespostaCTO= simulacaoService.realizarSimulacao(simulacao);
        return simulacaoRespostaCTO;
    }
}
