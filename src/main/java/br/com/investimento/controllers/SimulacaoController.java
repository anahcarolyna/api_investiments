package br.com.investimento.controllers;

import br.com.investimento.models.Simulacao;
import br.com.investimento.service.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> buscarTodas(){
        Iterable<Simulacao> simulacoes = simulacaoService.consultarTodos();

        return simulacoes;
    }
}
