package br.com.investimento.service;

import br.com.investimento.models.TipoInvestimento;
import br.com.investimento.repositories.TipoInvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TipoInvestimentoService {

    @Autowired
    private TipoInvestimentoRepository tipoInvestimentoRespository;


    public TipoInvestimento salvarInvestimento(TipoInvestimento tipoInvestimento){
       if(!tipoInvestimentoRespository.existsByNome(tipoInvestimento.getNome())) {
           TipoInvestimento tipoInvestimentoObjeto = tipoInvestimentoRespository.save(tipoInvestimento);
           return tipoInvestimentoObjeto;
       }
       else
       throw new RuntimeException("Já existe investimento cadastrado com esse nome!");
    }

    public Iterable<TipoInvestimento> consultarTodosInvestimentos(){
        Iterable<TipoInvestimento> tipoInvestimentos = tipoInvestimentoRespository.findAll();
        return tipoInvestimentos;
    }

    public TipoInvestimento buscarPorId(int id){
        Optional<TipoInvestimento> optionalInvest = tipoInvestimentoRespository.findById(id);
        if(optionalInvest.isPresent()){
            return optionalInvest.get();
        }

        throw new RuntimeException("Favor informar um investimento válido!");
    }
}
