package br.com.investimento.service;

import br.com.investimento.models.Simulacao;
import br.com.investimento.models.dtos.SimulacaoRespostaCTO;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {

    @Autowired
    SimulacaoRepository simulacaoRepository;


    public Iterable<Simulacao> consultarTodos(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }

    public SimulacaoRespostaCTO realizarSimulacao(Simulacao simulacao){
        simulacaoRepository.save(simulacao);

        SimulacaoRespostaCTO simulacaoRespostaCTO = new SimulacaoRespostaCTO();
        double montante = simulacao.getValorAplicado();

        for (int tempo1 = 1; tempo1 <= simulacao.getQuantidadeMeses(); tempo1++) {
            montante += simulacao.getTipoInvestimento().getRendimentoAoMes() * montante;
        }

        simulacaoRespostaCTO.setMontante(montante);
        simulacaoRespostaCTO.setRendimentoPorMes(simulacao.getTipoInvestimento().getRendimentoAoMes());

        return simulacaoRespostaCTO;
    }
}
