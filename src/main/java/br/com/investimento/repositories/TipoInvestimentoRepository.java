package br.com.investimento.repositories;

import br.com.investimento.models.TipoInvestimento;
import org.springframework.data.repository.CrudRepository;

public interface TipoInvestimentoRepository extends CrudRepository<TipoInvestimento, Integer> {
    Iterable<TipoInvestimento> findByNome(String nome);
    Boolean existsByNome(String nome);
}
